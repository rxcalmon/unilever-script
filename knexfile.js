/**
 * @typedef {import('knex').Config} KnexConfig
 * 
 * @typedef {Object} KnexFile
 * @property {KnexConfig} development
 * @property {KnexConfig} test
 * @property {KnexConfig} staging
 * @property {KnexConfig} production
 */


/**
 * @type {KnexFile}
 */
module.exports = {
  development: {
    client: 'sqlite',
    connection: {
      filename: './src/database/db.sqlite'
    },
    migrations: {
      directory: './src/database/migrations'
    },
    useNullAsDefault: true
  },
  production: {
    client: 'sqlite',
    connection: {
      filename: '../../Unilever-SJC-APP_Unilever_Backend/src/database/db.sqlite'
    },
    migrations: {
      directory: '../../Unilever-SJC-APP_Unilever_Backend/src/database/migrations'
    },
    useNullAsDefault: true
  }
}