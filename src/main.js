const csvParser = require('csv-parser');
const XLSX = require('xlsx');
const glob = require('glob');
const fs = require('fs');
const ReadStreamPayslips = require('./events/readStreamPayslips');
const ReadStreamUsersData = require('./events/readStreamUsersData');
const xlsxToCsv = require('./utils/xlsxToCsv');
const deleteCsv = require('./utils/deleteCsv');

const {
    PAYSLIP_PATTERN,
    USER_DATA_PATTERN,
} = require('./utils/constants');

const LogHandler = require('./logHandler');


class Main {
    constructor() {
        this.payslipStream = new ReadStreamPayslips();
        this.usersStream = new ReadStreamUsersData();
        this.tablePathProvided = ''
    }

    generatePayslips({
        tablePath
    }) {
        this.logHandler = new LogHandler(tablePath);
        this.tablePathProvided = `${tablePath}/${PAYSLIP_PATTERN}*.csv`;
        try {
            const fileNames = glob.sync(this.tablePathProvided);

            if (fileNames.length === 0) {
                this.logHandler.general(new Error('No documents found!'));
                console.log('No documents found. See ya!\n-------------------------');
                process.exit();
            }

            new Promise((resolve, reject) => {
                fileNames.reduce((fileNamesPromise, fileName, index) => {
                    const fileNameCleaned = fileName.replace(`${tablePath}/`, '');
                    return fileNamesPromise.then(() =>
                        new Promise((resolveFileName, rejectFileName) => {
                            fs.createReadStream(fileName)
                                .pipe(csvParser({}))
                                .on('data',
                                    row =>
                                        this.payslipStream.serializeRow(row, (rowError) =>
                                            this.logHandler.file('Warning', rowError, PAYSLIP_PATTERN, fileNameCleaned)
                                        ))
                                .on('end',
                                    async () => {
                                        await this.payslipStream.persistFile()
                                            .catch(error => {
                                                this.logHandler.file('Error', error, PAYSLIP_PATTERN, fileNameCleaned);
                                                throw error;
                                            });
                                        await this.moveFileSuccess(fileNameCleaned, tablePath)
                                            .catch(error => {
                                                this.logHandler.file('Error', error, PAYSLIP_PATTERN, fileNameCleaned);
                                                throw error;
                                            });
                                        this.logHandler.file('Success', 'Moved to history!', PAYSLIP_PATTERN, fileNameCleaned);
                                        resolveFileName();
                                        if (index === fileNames.length - 1)
                                            resolve();
                                    }
                                )
                                .on('error',
                                    error => {
                                        this.logHandler.file('Error', error, PAYSLIP_PATTERN, fileNameCleaned);
                                        throw error;
                                    }
                                );
                        }))
                }, Promise.resolve([]));
            }).then(() => {
                console.log("Ending script. See ya!\n-------------------------");
                process.exit();
            });
        } catch (error) {
            this.logHandler.general(error);
            throw error;
        }

    }

    async generateUsersData({
        tablePath,
    }) {
        this.logHandler = new LogHandler(tablePath);
        this.tablePathProvided = `${tablePath}/*${USER_DATA_PATTERN}*.*`;
        try {
            const fileNames = glob.sync(this.tablePathProvided);
            if (fileNames.length === 0) {
                this.logHandler.general(new Error('No documents found!'));
                console.log('No documents found. See ya!\n-------------------------');
                process.exit();
            }
            new Promise((resolve, reject) => {
                fileNames.reduce((fileNamesPromise, fileName, index) => {
                   
                    return fileNamesPromise.then(() =>
                        new Promise((resolveFileName, rejectFileName) => {
                            const csvFile = !fileName.includes('.csv') ? xlsxToCsv(fileName) : fileName;
                            const fileNameCleaned = csvFile.replace(`${tablePath}/`, '');
                            fs.createReadStream(csvFile)
                                .pipe(csvParser())
                                .on('data',
                                    async (row) =>
                                        await this.usersStream.serializeRow(row, (rowError) =>
                                            this.logHandler.file('Warning', rowError, USER_DATA_PATTERN, fileName)
                                        ))
                                .on('end',
                                    async () => {
                                        await this.usersStream.insertNewUsers()
                                            .catch(error => {
                                                this.logHandler.file('Error', error, USER_DATA_PATTERN, fileNameCleaned);
                                                throw error;
                                            });
                                        await this.usersStream.updateUsers()
                                            .catch(error => {
                                                this.logHandler.file('Error', error, USER_DATA_PATTERN, fileNameCleaned);
                                                throw error;
                                            });
                                        await this.moveFileSuccess(fileNameCleaned, tablePath)
                                            .catch(error => {
                                                this.logHandler.file('Error', error, USER_DATA_PATTERN, fileNameCleaned);
                                                throw error;
                                            });
                                        this.logHandler.file('Success', 'Moved to history!', USER_DATA_PATTERN, fileNameCleaned);
                                        resolveFileName();
                                        if (index === fileNames.length - 1)
                                            resolve();
                                    }
                                )
                                .on('error',
                                    error => {
                                        this.logHandler.file('Error', error, PAYSLIP_PATTERN, fileNameCleaned);
                                        throw error;
                                    }
                                );
                        }))
                }, Promise.resolve([]));
            }).then(() => {
                console.log("Ending script. See ya!\n-------------------------");
                process.exit();
            });
        } catch (error) {
            this.logHandler.general(error);
            throw error;
        }
    }

    async moveFileSuccess(filename, tablePath) {
        if (!await fs.existsSync(`${tablePath}/history`))
            await fs.mkdirSync(`${tablePath}/history`)
        await fs.rename(
            `${tablePath}/${filename}`,
            `${tablePath}/history/${filename}`,
            (err) => {
                if (err)
                    throw err.toString();

            })
    }
}

module.exports = new Main();