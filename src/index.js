"use strict"
const yargs = require('yargs');
const main = require('./main');

const setPositionalCommand = (yargs) => {
  yargs
    .positional('tablePath', {
      describe: 'path where the table to be converted are',
      default: process.env.TABLE_PATH || ''
    })
}

(() => {
  console.log('-------------------------\nStarting script. Welcome!');
  yargs
    .command(
      'payslips [tablePath]', 'convert payslip table to BD',
      setPositionalCommand,
      main.generatePayslips.bind(main)
    ).command(
      'users_data [tablePath]', 'convert DT4HR table to BD',
      setPositionalCommand,
      main.generateUsersData.bind(main)
    ).argv;

  const handleError = error => {
    console.log(`Error: ${error instanceof String ? error.substring(0,64): error.toString().substring()}...\nError on calling script. Sorry!\n-------------------------`);
    process.exit();
  }

  process
    .on('uncaughtException', handleError)
    .on('unhandledRejection', handleError);
})();