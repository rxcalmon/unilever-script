const {
    v4: uuid
} = require('uuid');

const repository = require('../database/repository/usersRepository')

const connection = require('../database/connection')

class ReadStreamUsersData {
    constructor() {
        this.csvUsersData = [];
        this.existUsers=[];
    }

    async serializeRow({

        CPF_COLAB: cpf,
        NOME_COLAB: name,
        DATA_NASC: birthday,
        COD_COLAB: re,
        COD_EMPRESA: company,
        COD_FILIAL: subsidiary,
        DATA_ADMISSAO: start_date,
        DATA_DEMISSAO: end_date,
        CARGOS: position

    }, callbackError) {
        if (
            !name||
            !cpf || cpf === "*" ||
            !birthday||
            !re ||
            !company ||
            !subsidiary ||
            !start_date ||
            !position
        ) {
            callbackError(`Required fields missing! \n{\ncpf:${cpf},\nname:${name},\nbirthday:${birthday},
            \nre:${re},\ncompany:${company},\nfilename:${subsidiary},\nstart_date:${start_date},\nposition:${position}\n}`);
        }

        try {

            this.csvUsersData.push({
                name,
                cpf,
                birthday,
                re,
                company,
                subsidiary,
                start_date,
                end_date,
                position
            });

        } catch (error) {
            callbackError(`Malformed fields! \n{\ncpf:${cpf},\nname:${name},\nbirthday:${birthday},
            \nre:${re},\ncompany:${company},\nfilename:${subsidiary},\nstart_date:${start_date},\nend_date:${end_date},\nposition:${position}\n}`);
        }

    }

    async insertNewUsers() {
        let newUsers =[];
        await this.csvUsersData.reduce(async (promisedUserData, userData) => {
            try{
                await promisedUserData
                const existUser = await repository.checkUserDB(userData);
                existUser && this.existUsers.push(userData);

                let isValid = true;
                for(const curentValue in userData){
                    if (!isValid) break;
                    isValid = "*" !== userData[curentValue];
                }
            
                if (!existUser && isValid) {
                    userData["id"] = uuid();
                    newUsers.push(userData);
                }
                       
            }catch(error){
                console.log(error);
                throw error;
            }
        },Promise.resolve([]))
        .then(() => {
            console.log(`Total users inserted: ${this.csvUsersData.length - this.existUsers.length}\nInserts Finished`);
        })

       newUsers.length > 0 && await repository.insertUsersToDB(newUsers);
    }


    async updateUsers() {

        await this.existUsers.reduce(async (promisedUserData, existUser) => {
            try{
                await promisedUserData
                for(const curentValue in existUser){
                    "*" === existUser[curentValue] && delete existUser[curentValue];
                }
                               
            }catch(error){
                console.log(error);
                throw error;
            }

            Object.keys(existUser).length > 0 && await repository.updateUsersToDB(existUser);

        },Promise.resolve([]))
        .then(()=>{
            console.log(`Total users updated: ${this.existUsers.length}\nUpdate Finished`);
            this.existUsers = [];
        })
        this.csvUsersData=[];
    }
}

module.exports = ReadStreamUsersData;
