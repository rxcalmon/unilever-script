const formatReFromFileName = require('../utils/formatReFromFileName');
const repository = require('../database/repository/payslipRepository')
const {
    ROUND_MATRIX_LIMIT
} = require('../utils/constants');
const {
    v4: uuid
} = require('uuid');

class ReadStreamPayslips {
    constructor() {
        this.csvPaySlips = [];
        this.matrix = [];
        this.numOfRows = 0;
    }

    async serializeRow({
        Workers: name,
        Attachment: filename,
        attachmentContent: document
    }, callbackError) {
        if (!name || name.endsWith('(On Leave)') || !filename || !document) {
            return callbackError(`Required fields missing! \n{\n\tname:${name},\n\tfilename:${filename},\n\tdocument:${typeof document !== "undefined"}\n}`);
        }

        try {
            const [user_re, date] = formatReFromFileName(filename);

            if (this.csvPaySlips.length < ROUND_MATRIX_LIMIT) {
                this.csvPaySlips.push({
                    id: uuid(),
                    filename,
                    user_name: name,
                    user_re,
                    date,
                    document,
                });
               
            }

            

            if (this.csvPaySlips.length === ROUND_MATRIX_LIMIT) {
                this.matrix.push(this.csvPaySlips);
                this.numOfRows += this.csvPaySlips.length;
                this.csvPaySlips = [];
            }
        } catch (error) {
            callbackError(`Malformed fields! \n{\n\tname:${name},\n\tfilename:${filename},\n\tdocument:${typeof document !== "undefined"}\n}`);
        }

    }

    async persistFile() {
        await this.matrix.reduce(async (promisedRoundRows, roundRows) => {
            await promisedRoundRows.then(async () => await repository.appendRowsToDB(roundRows));
        }, Promise.resolve([]))
        .then(async () => {
            if(this.csvPaySlips.length > 0) await repository.appendRowsToDB(this.csvPaySlips);
        })
        .then(() => {
            this.numOfRows += this.csvPaySlips.length;
            console.log(`Finished\nTotal registered: ${this.numOfRows}`);
            this.csvPaySlips.length = [];
            this.matrix = [];
            this.numOfRows = 0;
        })
        .catch( error => {
            throw error.toString();
        })
    }
}

module.exports = ReadStreamPayslips;