const fs = require('fs');
const moment = require('moment');

class LogHandler {

    constructor(tablePath) {
        this.today = moment().format('YYYYMMDD');
        if (!fs.existsSync(`${tablePath}/logs`)) {
            fs.mkdirSync(`${tablePath}/logs`)
        }
        this.logPath = `${tablePath}/logs`;
    }

    general(error) {
        const errorMsg = error.toString().length > 128 ? `${error.toString().substring(0,128)}...` : error.toString();
        fs.appendFileSync(
            `${this.logPath}/general_error_${this.today}.txt`,
            `${moment().format('HH:mm')} - ${errorMsg}\n`,
            "UTF-8", {
                flags: `a+`
            })

    }

    file(alertType, error, actionType, fileName, ) {
        const errorMsg = error.length > 128 ? `${error.substring(0,128)}...` : error;
        fs.appendFileSync(
            `${this.logPath}/${actionType}_${this.today}.txt`,
            `${alertType} - ${moment().format('HH:mm')} - ${fileName.replace(`${this.tablePath}/`,'')}: ${errorMsg}\n`,
            "UTF-8", {
                flags: `a+`
            })
    }
}

module.exports = LogHandler;