const knex = require('knex')

const cfg = require('../../knexfile')

const { NODE_ENV = 'development' } = process.env

const connection = knex(cfg[NODE_ENV])

module.exports = connection