/**
 * @typedef {import('knex')} Knex
 */

/**
 * @param {Knex} knex
 */
exports.up = knex => {
  return knex.schema.createTableIfNotExists('payslips', table => {
    table.uuid('id').primary()
    table.string('filename', 128).notNullable()
    table.string('user_name', 128).notNullable()
    table.string('user_re', 9).notNullable()
    table.string('date', 6).notNullable()
    table.json('document').notNullable()
  })
};

/**
 * @param {Knex} knex
 */
exports.down = knex => {
  return knex.schema.dropTableIfExists('payslips')
};
