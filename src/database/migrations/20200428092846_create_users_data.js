/**
 * @typedef {import('knex')} Knex
 */

/**
 * @param {Knex} knex
 */
exports.up = knex => {
  return knex.schema.createTableIfNotExists('users', table => {
    table.uuid('id').primary()
    table.string('re', 9).unique().notNullable()
    table.string('cpf', 11).unique().notNullable()
    table.string('name', 128).notNullable()
    table.date('birthday').notNullable()
    table.date('start_date').notNullable()
    table.date('end_date')
    table.string('company', 128).notNullable
    table.string('subsidiary', 128).notNullable
    table.string('position', 128).notNullable
    table.string('rg', 9)
    table.string('password', 255)
  })
};

/**
 * @param {Knex} knex
 */
exports.down = knex => {
  return knex.schema.dropTableIfExists('users')
};
