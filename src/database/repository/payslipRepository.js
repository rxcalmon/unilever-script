const connection = require('../connection')

class PayslipRepository {
    constructor() {

    }

    async appendRowsToDB(rows) {
        await connection('payslips').insert(rows).timeout(3600000);
        const totalRows = (await connection('payslips').count()).pop()['count(*)'];
        console.log(`total rows: ${totalRows}`);
    }
}

module.exports = new PayslipRepository();