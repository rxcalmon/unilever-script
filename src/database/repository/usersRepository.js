
const connection = require('../connection')

class UsersRepository {
    
    async checkUserDB(row) {
        const [exist] = await connection('users').where({ cpf:row.cpf }).select('*');
        return exist;
    }

    async insertUsersToDB(rows) {
        await connection('users').insert(rows).timeout(300000);
    }

    async updateUsersToDB(row) {
        await connection('users').update(row).where({cpf:row.cpf}).timeout(300000);
    }


}

module.exports = new UsersRepository();