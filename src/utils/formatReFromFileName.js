/**
 * @param {string} re 
 */
const formatReFromFileName = re => {
  const formattedRe = re.substring(0, 9)

  const formatRe = (value) => {
    if (value.length - value.lastIndexOf('_') === 1) {
      const newValue = value.split('')

      newValue.pop()
      newValue.unshift('0')

      return newValue.join('').length == 9
        ? newValue.join('')
        : null
    }

    return value
  }

  const formatDate = (value) => {
    const splittedValue = value.split('_')

    return splittedValue[1] === 'EURONET'
      ? splittedValue[2]
      : splittedValue[1]

  }

  return [formatRe(formattedRe), formatDate(re)]
}

module.exports = formatReFromFileName