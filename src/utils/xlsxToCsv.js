const XLSX = require('xlsx');
const fs = require('fs');
const deleteCsv = require('./deleteCsv')

const xlsxToCsv = fileName => {
    const workbook = XLSX.readFile(fileName);
    const sheetNames = workbook.SheetNames;
    const sheetIndex = 1;
    const data = XLSX.utils.sheet_to_csv(workbook.Sheets[sheetNames[sheetIndex-1]],{blankrows:false})
    const csvFile = `${fileName.replace('xlsx','').replace('xls','')}csv`;
    fs.writeFileSync(csvFile, data);
    const fileNamePieces = csvFile.split(/[\s\/]+/);
    console.log(`${fileNamePieces[fileNamePieces.length-1]} created!`);
    deleteCsv(fileName)

    return csvFile;
}

  module.exports = xlsxToCsv
