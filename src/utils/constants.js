module.exports = {
    PAYSLIP_PATTERN: 'INT017_Workday_Payslips',
    USER_DATA_PATTERN: 'Brazil_DT4HR',
    ROUND_MATRIX_LIMIT: 151
}