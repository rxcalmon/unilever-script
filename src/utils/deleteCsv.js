
const fs = require('fs');

const deleteCsv = file => {
    fs.unlink(file, function (err) {
        if (err) throw err;
        const fileNamePieces = file.split(/[\s\/]+/);
        console.log(`${fileNamePieces[fileNamePieces.length-1]} deleted!`);
    }); 

}
module.exports = deleteCsv